'use strict'
const axios = require('axios')

const searchNews = (company, apiKey) => {
  const apiUrl = `https://api.cognitive.microsoft.com/bing/v7.0/news/search`

  const config = {
    params: { 
      q : company
      ,count: 10
      ,offset: 0
      ,mkt :'en-us'
      ,safeSearch : 'Moderate' 
    },
    headers: { 'Ocp-Apim-Subscription-Key': apiKey }
  }

  return axios.get(apiUrl, config).then((result) => {
    return {
     news :  result.data.value
    }
  }).catch((error)=>{
    return {"message" :"bad api call"}
  })
}

module.exports = searchNews
