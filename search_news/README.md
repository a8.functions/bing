# search_news

Searches Bing news for a given key work

## Examples

### CLI

`a8 invoke bing.search_news  '{"apiKey":"","term":"google"}'`

## Input

`apiKey` is a Microsoft API key for Bing Serach APIs v7

``` json
{
    apiKey : string,
    term: string
}
```

## Output

Exmple output:

``` json
 {
    news: [{
    "name": "Microsoft Confirms Change To Windows 10 Passwords That Nobody Saw Coming",
    "url": "https:\/\/www.forbes.com\/sites\/daveywinder\/2019\/04\/27\/microsoft-confirms-change-to-windows-10-passwords-that-nobody-saw-coming\/",
    "image": {
      "thumbnail": {
        "contentUrl": "https:\/\/www.bing.com\/th?id=ON.F8043ECA767EB37E4272C353C64D716F&pid=News",
        "width": 600,
        "height": 315
      }
    },
    "description": "Ask a bunch of security professionals what makes a secure password and you'll get a bunch of different answers. Some will argue that it's all about length, others that randomness and complexity are king while everyone will agree that password reuse is ...",
    "about": [{
      "readLink": "https:\/\/api.cognitive.microsoft.com\/api\/v7\/entities\/16aeb6d9-9098-0a40-4970-8e46a4fcee12",
      "name": "Microsoft Windows"
    }, {
      "readLink": "https:\/\/api.cognitive.microsoft.com\/api\/v7\/entities\/a093e9b9-90f5-a3d5-c4b8-5855e1b01f85",
      "name": "Microsoft"
    }],
    "provider": [{
      "_type": "Organization",
      "name": "Forbes",
      "image": {
        "thumbnail": {
          "contentUrl": "https:\/\/www.bing.com\/th?id=AR_f798ef715781699feff0f3ea5f101626&pid=news"
        }
      }
    }],
    "datePublished": "2019-04-27T09:14:00.0000000Z",
    "category": "ScienceAndTechnology"
  }...]
 }
```