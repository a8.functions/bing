const fdk=require('@autom8/fdk');
const a8=require('@autom8/js-a8-fdk')
const searchNews=require('./searchNews')

fdk.handle(function(input){

  if(!input.apiKey){
    return { message: 'Please include an api key (apiKey)'}
  }

  if(!input.term){
    return { message: 'Pleaes include a search term (term)'}
  }

  return searchNews(input.term, input.apiKey)
})
